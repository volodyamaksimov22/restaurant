# Restaurant

Тестовое задание на позицию Frontend разработик (Vue)

## Описание тестового задания

Создать SPA VUE3 на фреймворке Quasar.

Схема дизайна приложена.
Все вводы должны иметь валидацию на Ваше усмотрение.

1. отобразить список блюд карточками, каждая карточка должна быть редактируемой
2. анкета регистрации гостя, все поля должны иметь валидацию
3. отобразить все поля списка гостей, для каждого гостя должна быть возможность добавить дополнительный номер телефона и адрес почты

////////////////////////
api для WebSocket
//////////////////////
SERVER API

Каждое сообщение, отправляемое на сервер должно быть формата JSON и содержать в себе ваш персональный идентификационный ключ и информацию об совершаемой операции.
Список операций и требуемые параметры к каждой операции данных:
dishes_list – получение списка блюд (key<string>)
guests_list – получение списка гостей (key<string>)
guest_regestration – регистрация нового гостя (key<string>, surname<string>, forename<string>, middlename<string>, phone<string>, email<string>, birthday<string>, address<string>)
change_dish_desc – изменение описания блюда (key<string>, id<int>, desc_value<string>)

change_dish_cost – изменение цены блюда (key<string>, id<int>, cost_value<float>)
add_guestphone – добавление гостю номера телефона (key<string>, id<int>, phone<string>)
add_guestemail – добавление гостю адреса электронной почты (key<string>, id<int>, email<string>)

Схема дизайна
![Схема.png](./Схема.png)


## Установка сервера

сервер на .NET

шаги по запуску сервера
 - Установить dotnet (версия 6.0, 7.0 не поддерживатется)
 - Перейти в папку server в терминале
 - Прописать в терминале команду chmod +x WebsocketPlusSQLite.dll
 - Запустить программу с помощью команды dotnet WebsocketPlusSQLite.dll

### Server API

Каждое сообщение, отправляемое на сервер должно быть формата JSON и содержать в себе ваш персональный идентификационный ключ и информацию об совершаемой операции.
Список операций и требуемые параметры к каждой операции данных:
 - **dishes_list** – получение списка блюд (key<string>)
 - **guests_list** – получение списка гостей (key<string>)

 - **guest_regestration** – регистрация нового гостя (key<string>, surname<string>, forename<string>, middlename<string>, phone<string>, email<string>, birthday<string>, address<string>)
 - **change_dish_desc** – изменение описания блюда (key<string>, id<int>, desc_value<string>)
 - **change_dish_cost** – изменение цены блюда (key<string>, id<int>, cost_value<float>)
 - **add_guestphone** – добавление гостю номера телефона (key<string>, id<int>, phone<string>)
 - **add_guestemail** – добавление гостю адреса электронной почты (key<string>, id<int>, email<string>)

Пример запроса к серверу:
```JSON
{
"operation":"change_dish_desc",
"key":"wSEA9BpRNhkwYY3tfOmS",
"id":1,
"desc_value":"Новое описание блюда"
}
```
