import { defineStore } from 'pinia';

export const useSocketStore = defineStore('socket-store', {
  state: () => ({
    socket: null,
    error: null,
    key: 'wSEYY3tA9BpRNhkwfOmS',
    dishes: [],
  }),
  actions: {
    createSocket(url) {
      const socket = new WebSocket(url)

      socket.onopen = () => {
        console.log(socket)
        this.$state.socket = socket
      }

      socket.onclose = (event) => {
        if (event.wasClean) {
          this.$state.socket = null
        } else {
          this.$state.error = 'Соединение было разорвано'
        }
      }

      socket.onerror = (e) => {
        this.$state.error = 'Соединение разорвано'
        console.log(e)
      }
    },
    getDishesList() {
      const body = JSON.stringify({
        key: this.$state.key,
      })
      this.$state.socket.onmessage = body
    },
  },
})
